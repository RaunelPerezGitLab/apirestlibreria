const app = require('./config/server');

require('./app/routes/books.js')(app);

//iniciando el servidor
app.listen(app.get('port'), ()=>{
    console.log('servidor corriendo en el puerto: ', app.get('port'));
});