const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

//insttancia de express
const app = express();

/////////////////SETTINGS///////////////////////////////
//fijar el puerto ya sea el dado por el SO ó si no usa el 3000
app.set('port', process.env.PORT || 3000);

//Especifica el motor de platillas que se va a utilizar
app.set('view engine','ejs');

//Dónde van a estar mis HTML's es decir las vistas , ejs's que al final se covierten en HTML, con path join el cual se encarga de unir dirctorios
app.set('views', path.join(__dirname, '../app/views'));

////////////////////////////////////////////////////////


////////////////////MIDLEWARE//////////////////////////

//solo pasaremos un formulario y no imágenes ni datos complejos por eso esta config url-encode
app.use(bodyParser.urlencoded({extended: false}));
///////////////////////////////////////////////////////

//finalmente exportamos este archivo de configuración
module.exports = app;