//Aquí es dónde se hará la conexión a la BDD
const mysql = require('mysql');

module.exports = () =>{
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '',
        database: 'libreria'
    });
}
