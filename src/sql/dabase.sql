create table libro(cve_lib int unsigned auto_increment not null primary key, titulo_lib varchar(120)not null, autor_lib varchar(120) not null, descripcion_lib text not null, editorial_lib varchar(80)not null, edicion_lib varchar(10) not null);

insert into libro values(null, 'Cien anios de soledad', 'Gabriel Garcia Marquez', 'Es impresionante la historia.', 'Diana', 'primera');