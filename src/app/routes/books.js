//En este archivo se van a definir las rutas para libros. (Agregar libros, mostrar la vista, etc)
//Aquí se hace la renderización de los datos

const dbConnection = require('../../config/dbConnection');

module.exports = app =>{
    const connection = dbConnection();
    app.get('/', (req, res) =>{
        connection.query('SELECT * FROM libro;', (err, result) =>{
            res.render('../views/books/books', {
                book: result
            });
        });
    });

    app.post('/booksxyz', (req,res) =>{
        const {titulo_lib, autor_lib, descripcion_lib, editorial_lib, edicion_lib}= req.body;
        connection.query('INSERT INTO libro SET?', {
            titulo_lib,
            autor_lib,
            descripcion_lib,
            editorial_lib,
            edicion_lib
        }, (err, result) => {
            res.redirect('/');
        });
    });

    app.post('/changes', (req,res) =>{
        const cambio = req.body;
        
        if(cambio['Modificar']){
            const cve_lib = cambio['Modificar'];
            connection.query('SELECT * FROM libro where cve_lib = ?',cve_lib, (err, result) =>{
                res.render('../views/books/modifyBooks', {
                    book: result
                });
            }, (err, result)=>{
                res.redirect('/');
            });
        }else{
            const cve_lib = cambio['Eliminar'];
            connection.query('DELETE FROM libro WHERE cve_lib = ?', cve_lib, (error, resul) => {
                res.redirect('/');
            });
        }     
    })
    
    app.post("/actualiza", (req, res) => {
        const {titulo_lib, autor_lib, descripcion_lib, editorial_lib, edicion_lib,cve_lib}= req.body;
        connection.query("UPDATE libro SET titulo_lib='"+titulo_lib+"',autor_lib='"+autor_lib+"', descripcion_lib='"+descripcion_lib+"', editorial_lib='"+editorial_lib+"', edicion_lib='"+edicion_lib+"' WHERE cve_lib='"+cve_lib+"';",(err, result) => {
            res.redirect('/');
        });
    });

}